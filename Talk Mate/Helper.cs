﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;

namespace Talk_Mate
{
    partial class MainPage
    {
        public void GetResponse(string input)
        {
            WebClient client = new WebClient();
            client.Headers["Content-Type"] = "application/x-www-form-urlencoded";
            Uri uri = new Uri("http://www.pandorabots.com/pandora/talk-xml");
            string custid = "";

            if (State.ContainsKey("custid"))
            {
                custid = State["custid"].ToString();
            }
            else
            {
                custid = new Random().Next(0, 10000).ToString() + DateTime.Now.Millisecond.ToString();
                State["custid"] = custid;
            }

            try
            {

                string data = "botid=ebb6ea0eee3426f4&custid=" + custid + "&input=" + input;
                client.UploadStringAsync(uri, data);

                sayButton.IsEnabled = false;
                clearButton.IsEnabled = false;
                inputBox.IsEnabled = false;

                client.UploadStringCompleted += new UploadStringCompletedEventHandler((sender, e) =>
                {
                    if (e.Error != null)
                    {
                        anna.Text = "Anna can not talk to you right now! Try again later!";
                    }

                    try
                    {
                        XmlReader reader = XmlReader.Create(new StringReader(e.Result));
                        Dictionary<string, string> retValues = new Dictionary<string, string>();
                        reader.ReadToFollowing("that");
                        string response = reader.ReadElementContentAsString();
                        anna.Text = response;
                        inputBox.Focus();
                    }
                    catch
                    {
                        anna.Text = "Communication Error! Are we online?";
                    }

                    sayButton.IsEnabled = true;
                    clearButton.IsEnabled = true;
                    inputBox.IsEnabled = true;

                });

            }
            catch (Exception e)
            {
                anna.Text = "Ooops! Error ocurred!";

            }
        }
    }
}
