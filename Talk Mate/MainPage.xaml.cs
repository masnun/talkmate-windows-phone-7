﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Talk_Mate
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

        }

        private void sayButton_Click(object sender, RoutedEventArgs e)
        {
            if (inputBox.Text != String.Empty)
            {
                this.GetResponse(inputBox.Text);
            }
            else
            {
                anna.Text = "You have nothing to say?";
            }

        }


        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            inputBox.Text = "";
            inputBox.Focus();
        }

               
    }
}